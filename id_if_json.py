import json


def take_id(jdict):
    if not isinstance(jdict, dict):
        return 0, False
    if "id" in jdict:
        return jdict["id"], True
    else:
        for k in jdict:
            res, is_res = take_id(jdict[k])
            if is_res:
                return res, True
    return 0, False

def id_if_json(myjson):
    try:
        jdict = json.loads(myjson)
        return take_id(jdict)
    except ValueError as e:
        return 0, False

while True:
    myjson = input()

    res, is_res = id_if_json(myjson)

    if is_res:
        print(res)
